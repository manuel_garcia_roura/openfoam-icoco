#!/bin/bash

# Exit if any command fails:
set -e

# thermalPisoFoam solver:
cd test/VVER-1000/thermalPisoFoam
./AllmeshSeq
echo -e "Run thermalPisoFoam..."
thermalPisoFoam > thermalPisoFoam.out
echo -e "Run ThermalPisoFoamProblem..."
pisofoam setup_no_feedback.ini > ThermalPisoFoamProblem.out
if ! diff -q -I ".*Exec.*" -I ".*Time.*" -I ".*PID.*" -I ".*.*" -I ".*ExecutionTime.*" ThermalPisoFoamProblem.out thermalPisoFoam.out; then
   diff -I ".*ExecutionTime.*" ThermalPisoFoamProblem.out thermalPisoFoam.out
   echo -e "\e[31mKO.\e[0m"
else
   echo -e "\e[32mOK.\e[0m"
fi
rm thermalPisoFoam.out ThermalPisoFoamProblem.out log.out pisofoam++.out config.out.ini
cd ../../..

# thermalPimpleFoam solver:
cd test/VVER-1000/thermalPimpleFoam
./AllmeshSeq
echo -e "Run thermalPimpleFoam..."
thermalPimpleFoam > thermalPimpleFoam.out
echo -e "Run ThermalPimpleFoamProblem..."
pimplefoam setup_no_feedback.ini > ThermalPimpleFoamProblem.out
if ! diff -q -I ".*Exec.*" -I ".*Time.*" -I ".*PID.*" -I ".*.*" -I ".*ExecutionTime.*" ThermalPimpleFoamProblem.out thermalPimpleFoam.out; then
   diff -I ".*ExecutionTime.*" ThermalPimpleFoamProblem.out thermalPimpleFoam.out
   echo -e "\e[31mKO.\e[0m"
else
   echo -e "\e[32mOK.\e[0m"
fi
rm thermalPimpleFoam.out ThermalPimpleFoamProblem.out log.out pimplefoam++.out config.out.ini
cd ../../..
