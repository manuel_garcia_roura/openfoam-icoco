#!/bin/bash

# Exit if any command fails:
set -e

# thermalPisoFoam solver:
cd thermalPisoFoam
wmake
cd ..

# thermalPimpleFoam solver:
cd thermalPimpleFoam
wmake
cd ..

# ICoCo library:
cd ICoCo
wmake libso
cd ..
