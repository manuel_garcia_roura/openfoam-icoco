#!/bin/bash

# Exit if any command fails:
set -e

# thermalPisoFoam solver:
cd thermalPisoFoam
rm -rf Make/linux* bin
cd ..

# thermalPimpleFoam solver:
cd thermalPimpleFoam
rm -rf Make/linux* bin
cd ..

# ICoCo library:
cd ICoCo
rm -rf Make/linux* lnInclude lib
cd ..
