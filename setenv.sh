#!/bin/bash

# Root directory:
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# thermalPisoFoam solver:
export PATH=$DIR/thermalPisoFoam/bin:$PATH

# thermalPimpleFoam solver:
export PATH=$DIR/thermalPimpleFoam/bin:$PATH

# ICoCo library:
export OPENFOAMICOCO_ROOT_DIR=$DIR/ICoCo
export LD_LIBRARY_PATH=$OPENFOAMICOCO_ROOT_DIR/lib:$LD_LIBRARY_PATH
